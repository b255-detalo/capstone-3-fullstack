import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function Login() {
    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {
        e.preventDefault();

    fetch('http://localhost:3000/users/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
            },
        body: JSON.stringify({
            email: email,
            password: password
            })
        })
        .then(res => res.json())
        .then(data => {
        if (typeof data.access !== 'undefined') {
        localStorage.setItem('token', data.access);
            retrieveUserDetails(data.access);

        Swal.fire({
            title: 'Login Successful',
            icon: 'success',
            text: 'Welcome back Boss!'
        });
        } else {
        Swal.fire({
            title: 'Authentication Failed',
            icon: 'error',
            text: 'Check your login details and try again.'
        });
        }
    })
    .catch(error => {
        console.error('Authentication failed:', error);
    });

    setEmail('');
    setPassword('');
}

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:3000/users/details', {
            headers: {
            Authorization: `Bearer ${token}`
        }
    })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });
        });
    };

    useEffect(() => {
    setIsActive(email !== '' && password !== '');
    }, [email, password]);

    if (user.isAdmin) {
        return <Navigate to="/admin" />;
    }

    return (
        (user.id !== null) ?
            <Navigate to="/products"/>
            :
            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <h2>Account Login</h2>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>
                <br></br>
                { isActive ?

                <Button variant="success" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="success" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
                }
            </Form>
    )
}
