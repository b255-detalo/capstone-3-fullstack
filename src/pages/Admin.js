import { Fragment, useEffect, useState } from 'react';
import React from 'react'

import Banner from '../components/Banner';
import AdminTable from '../components/AdminTable';
import ProductCard from '../components/Product';
import { Link } from 'react-router-dom';


export default function Admin() {

		const [products, setProducts] = useState([])

		useEffect(() => {

				fetch(`${process.env.REACT_APP_API_URL}/products/all`)
				.then(res => res.json())
				.then(data => {
				    
				    console.log(data);

				    setProducts(data.map(product => {
				        return (
				            <AdminTable key={product._id} productProp={product}/>
				        );
				    }));
				});
		    }, []);

		return(
			<Fragment>
			<div className="text-center">
			  <h1>Admin Dashboard</h1>
			  <Link className="btn btn-info m-2" block="true" to="/admin/productCreate/products">
			    Add Product
			  </Link>
			 </div>
				{products}
			</Fragment>
		)
	}
