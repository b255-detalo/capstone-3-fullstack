import { Fragment } from 'react';
import Banner from '../components/Banner';
import React from 'react';
import { Button } from 'react-bootstrap';
import Highlights from '../components/Highlights';
import ProductCard from '../components/Product';
import { Link } from 'react-router-dom';

export default function Home() {
  const data = {
    title: "Starmate Store",
    content: `"Unleash your gaming experience"`,
    destination: "/products",
  };

  return (
    <Fragment>
      <div className="text-center">
        <Banner data={data} />
        <Link className="btn btn-success m-2" block="true" to="/products">
          Shop Now!
        </Link>
        <Highlights />
      </div>
    </Fragment>
  );
}