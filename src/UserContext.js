import React from 'react';

// Creates a context object
// A context object as the name state is adata type of an object that can be used to store informationthat can be shared to other components within the app.
// The context object is a different approach to passing information between components and allows weasier acces by avoiding the use of prop-drilling.

const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use the context object an d supply the necessarrry informaton needed to the context object
export const UserProvider =  UserContext.Provider;

export default UserContext;
