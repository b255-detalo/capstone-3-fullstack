import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductCreate(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState('');

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

		function productCreate(e) {
            e.preventDefault();

			fetch(`${ process.env.REACT_APP_API_URL }/products/`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem("token")}`
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price,
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(typeof data !== "undefined"){

                        setName('');
                        setDescription('');
                        setPrice('');

                        Swal.fire({
                            title: 'Successful!',
                            icon: 'success',
                            text: `Product ${name} ${description} successfully added`
                        });
                        navigate(`/admin`);
                    } else {

                        Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });
                    };
                })
                .catch(error => {
                    console.error('Error:', error);
                })
            };

useEffect(() => {

    if((name !== '' && description !== '' && price !== '' )){
        setIsActive(true);
    } else {
        setIsActive(false);
    }

}, [name, description, price]);

	return(
		<Form onSubmit={(e) => productCreate(e)}>
            <Form.Group controlId="productName">
                <Form.Label>Product Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter product name"
                    value={name} 
                    onChange={e => setName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter product description"
                    value={description} 
                    onChange={e => setDescription(e.target.value)}
                    required
                />
            </Form.Group>

	        <Form.Group controlId="price">
	            <Form.Label>Product Price (&#8369;)</Form.Label>
	            <Form.Control 
	                type="number" 
	                placeholder="Enter price"
	                value={price}
	                onChange={e => setPrice(e.target.value)} 
	                required
	            />
	        </Form.Group>

	        {isActive ? 
		        <Button variant="success" type="submit" id="submitBtn">
		        	Submit
		        </Button>
		        :
		        <Button variant="success" type="submit" id="submitBtn" disabled>
		        	Submit
		        </Button>
	    	}

	    </Form>
	)
}