import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView(){
	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const { productId } = useParams();


	const [quantity, setQuantity] = useState(1);
	// Using the state hook returns an array with the first elemen t being a value and the secodn element as function thats used to change the value of the first element

	const handleDecrement = () => {
		if(quantity > 1){
			setQuantity(prevCount => prevCount - 1);
		}
	}

	const handleIncrement = () => {
		if(quantity < 100){
			setQuantity(prevCount => prevCount + 1);
		}
	}


	const checkout = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					productId: productId
				})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Successful Checkout!",
					icon: 'success',
					text: 'You have successfully checkout this product!'
				})

				navigate("/products")
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId)

		fetch(`${process.env.REACT_APP_API_URL}/courses/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	return(
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            <Card.Subtitle>Quantity:</Card.Subtitle>

                            <div className="d-flex justify-content-center">
                                <button className="btn btn-success" onClick={handleDecrement}>-</button>
                                <div className="form-control text-center mx-2">{quantity}</div>
                                <button className="btn btn-success" onClick={handleIncrement}>+</button>
                             </div>

                             <br></br>      
                            { user.id !== null ?
                            	<Button variant="primary" block onClick={() => checkout(productId)}>Checkout</Button>
                            	:
                            	<Link className="btn btn-warning btn-block" to="/login">Log in to Purchase</Link>
                      		}
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
        </Container>
	)
}
