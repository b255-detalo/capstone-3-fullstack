import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

export default function AdminTable({ productProp }) {
    const { name, description, price, isActive, _id } = productProp;

    console.log(productProp)

    return (
      <Container className="text-center">
      <Row>
      <Col lg={{ span: 10, offset: 1 }}>
      <Card >
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>₱ {price}</Card.Text>
          <Card.Subtitle>In stock?</Card.Subtitle>
          <Card.Text className="m-2">{isActive ? 'Yes' : 'No'}</Card.Text>

          <Link className="btn btn-success m-1" block="true" to={`/admin/productUpdate/${_id}`}>
            Update
          </Link>

          <Link className="btn btn-success m-1" block="true" to={`/admin/productArchive/${_id}/archive`}>
            Deactivate
          </Link>

          <Link className="btn btn-success m-1" block="true" to={`/admin/productActivate/${_id}/activate`}>
            Activate
          </Link>

        </Card.Body>
      </Card>
      </Col>
      </Row>
      </Container>
    );
  }


AdminTable.propTypes = {
      product: PropTypes.shape({
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      isActive: PropTypes.bool.isRequired,
      _id: PropTypes.string.isRequired,
    }),
  };