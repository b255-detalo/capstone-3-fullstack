import { Row, Col, Card } from "react-bootstrap";
// import shoes from '../images/shoes.jpg';
// import tshirt from '../images/tshirt.jpg';
// import etc from '../images/etc.jpg';

export default function Highlights() {
  return (
    <Row className="mt-3 mb-3">
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          {/* <Card.Img variant="top" src={image} alt="Image" /> */}
          <Card.Body>
            <Card.Title>
              <h2>Experience</h2>
            </Card.Title>
            <Card.Text>
              Unlock Your Gaming Potential with Starmate Store's
              High-Performance GPUs!" Experience the thrill of gaming like never
              before. Fuel your passion with cutting-edge graphics cards from
              Starmate Store. Elevate your gameplay and unleash the full
              potential of your favorite titles. Level up your skills and
              dominate the virtual world with our top-of-the-line GPUs. Choose
              Starmate Store and turn your gaming passion into a remarkable
              experience!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          {/* <Card.Img variant="top" src={image} alt="Image" /> */}
          <Card.Body>
            <Card.Title>
              <h2>Next Level</h2>
            </Card.Title>
            <Card.Text>
              "Follow Your Passion, Fuel Your Victory - Starmate Store has the
              Perfect GPU for You!" Passion drives greatness, and at Starmate
              Store, we believe in empowering gamers to achieve their dreams.
              Whether you're a casual player or an eSports enthusiast, our
              diverse range of gaming GPUs will match your passion and ambition.
              Immerse yourself in stunning visuals, seamless performance, and
              unbeatable speed. Choose Starmate Store, where your gaming passion
              finds its perfect match!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          {/* <Card.Img variant="top" src={image} alt="Image" /> */}
          <Card.Body>
            <Card.Title>
              <h2>Gaming</h2>
            </Card.Title>
            <Card.Text>
              "Game On, Pursue Your Passion - Starmate Store's GPUs Unleash
              Infinite Possibilities!" Don't let anything hold you back from
              pursuing your gaming passion. Starmate Store's remarkable GPUs
              open up a world of limitless possibilities. Immerse yourself in
              lifelike graphics, fast frame rates, and smooth gameplay. Our
              cutting-edge technology will take your gaming experience to new
              heights, fueling your passion and igniting your competitive
              spirit. Choose Starmate Store and unlock the door to gaming
              excellence!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
