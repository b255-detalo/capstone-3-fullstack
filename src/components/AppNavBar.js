import Container from 'react-bootstrap/Container';
import { Fragment, useContext } from 'react';
import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar(){
	const { user } = useContext(UserContext);

	console.log(user);

	return(
		<Navbar classname="navbar" expand="lg">
		    <Container fluid >
		        <Navbar.Brand className='text-success' as={Link} to="/">Starmate Store</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="mr-auto">
		            <Nav.Link as={NavLink} to="/">Home</Nav.Link>

		            {(user.isAdmin === true) ?
		            <Nav.Link as={NavLink} to="/admin">Admin Dashboard</Nav.Link>
		            :
		            <Nav.Link as={NavLink} to="/products" exact="true">Products</Nav.Link>
		       		}

		            {(user.id !== null) ?
		            <Nav.Link as={NavLink} to="/logout" exact="true">Logout</Nav.Link>
		            :
		            <React.Fragment>
		            	<Nav.Link as={NavLink} to="/login" exact="true">Login</Nav.Link>
		            	<Nav.Link as={NavLink} to="/register" exact="true">Register</Nav.Link>
		            </React.Fragment>
		        	}
		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
	)
}