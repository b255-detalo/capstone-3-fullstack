import { useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

    export default function ProductActivate() {
        const { user } = useContext(UserContext);
        const navigate = useNavigate();
        const { productId } = useParams();

    const handleSubmit = (e) => {
        e.preventDefault();

    // Archive the product
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        })
        .then((res) => res.json())
        .then((data) => {
          console.log('archiveData', data);

        if (data === true) {
          Swal.fire({
            title: 'Product activated successfully!',
            icon: 'success',
            text: 'The product has been activated',
        });
        navigate(`/admin`);
        } else {
            Swal.fire({
              title: 'Something went wrong',
              icon: 'error',
              text: 'Please try again',
        });
        }
        })
        .catch((error) => {
        console.log(error);
         });
      };

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body>
              <Card.Title>Sure you want to activate this product?</Card.Title>
              <Button variant="success" type="submit" block="true" onClick={handleSubmit}>
                Activate Product
              </Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}